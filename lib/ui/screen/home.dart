import 'package:animations/animations.dart';
import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/screen/details.dart';
import 'package:finance_app/ui/screen/widget/cards/optionsCard.dart';
import 'package:finance_app/ui/screen/widget/cards/pieCard.dart';
import 'package:finance_app/ui/screen/widget/cards/plotCard.dart';
import 'package:finance_app/ui/screen/widget/movement/movementList.dart';
import 'package:finance_app/ui/screen/widget/painters/shapePaint.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return GetBuilder(
      init: FinanceBloc(),
      builder: (FinanceBloc controller) => Scaffold(
        body: Container(
          height: size.height,
          width: size.width,
          child: Stack(
            fit: StackFit.loose,
            alignment: Alignment.center,
            children: [
              CustomPaint(
                foregroundPainter: ShapePaint(),
                child: Container(),
              ),
              Positioned(
                top: size.height * .10,
                left: size.width * .03,
                child: OpenContainer(
                  openBuilder: (context, action) =>
                      Container(height: 90, child: DetailsPage()),
                  closedElevation: 13,
                  closedShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  closedBuilder: (context, action) => Container(
                    height: size.height * .25,
                    width: size.width * .4,
                    child: PlotCard(),
                  ),
                ),
              ),
              Positioned(
                top: size.height * .10,
                right: size.width * .03,
                child: OpenContainer(
                  openBuilder: (context, action) => DetailsPage(),
                  closedElevation: 13,
                  closedShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  closedBuilder: (context, action) => Container(
                    height: size.height * .25,
                    width: size.width * .4,
                    child: PieCard(),
                  ),
                ),
              ),
              Positioned(
                top: size.height * .4,
                child: Container(
                  height: size.height * .25,
                  width: size.width * .9,
                  child: OptionsCard(),
                ),
              ),
              Positioned(top: size.height * .67, child: MovementList())
            ],
          ),
        ),
      ),
    );
  }
}
