import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/constants.dart';
import 'package:finance_app/ui/screen/widget/movement/itemMovement.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MovementList extends StatefulWidget {
  @override
  _MovementListState createState() => _MovementListState();
}

class _MovementListState extends State<MovementList>
    with SingleTickerProviderStateMixin {
  final FinanceBloc _financeBloc = Get.find<FinanceBloc>();
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
    _financeBloc.key = GlobalKey<AnimatedListState>();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.centerLeft,
      height: size.height * .35,
      width: size.width * .8,
      child: Obx(() {
        _financeBloc.movements.sort((a, b) => b.date.compareTo(a.date));
        return _financeBloc.movements.isNotEmpty
            ? AnimatedList(
                key: _financeBloc.key,
                initialItemCount: _financeBloc.movements.length,
                itemBuilder: (context, index, animation) => SlideTransition(
                    position:
                        Tween<Offset>(begin: Offset(-1, 0), end: Offset(0, 0))
                            .animate(CurvedAnimation(
                                parent: animation,
                                curve: Curves.easeInOutExpo)),
                    child: ItemMovement(
                      item: _financeBloc.movements[index],
                    )),
              )
            : Center(
                child: Text(
                  'no hay datos aun.',
                  style: AppStyle.headerFinance,
                ),
              );
      }),
    );
  }
}
