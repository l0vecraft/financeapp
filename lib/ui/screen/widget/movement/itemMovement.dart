import 'package:finance_app/core/models/transaccion.dart';
import 'package:finance_app/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ItemMovement extends StatelessWidget {
  final Transaccion item;
  ItemMovement({this.item});
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: size.height * .015, horizontal: size.width * .015),
      child: Container(
        padding: EdgeInsets.symmetric(
            vertical: size.height * .01, horizontal: size.width * 0.025),
        height: size.height * .13,
        width: size.width * .5,
        decoration: BoxDecoration(
            color: Colors.white10,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                blurRadius: 7,
                spreadRadius: 1,
                color: Colors.black,
                offset: Offset(0, 3),
              ),
              BoxShadow(
                blurRadius: 1,
                spreadRadius: 1,
                color: Colors.white,
                offset: Offset(0, -2),
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${item.title}',
                  style: AppStyle.movemtnTitle,
                ),
                Text(
                  '${item.category}',
                  style: AppStyle.movemtnTitle,
                ),
              ],
            ),
            SizedBox(height: size.height * .01),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${item.description}',
                  style: AppStyle.movementDesc,
                ),
                Text(
                  item.type == "incoming"
                      ? '\$ ${NumberFormat().format(item.quantity)}'
                      : '- \$ ${NumberFormat().format(item.quantity)}',
                  style: item.type == "incoming"
                      ? AppStyle.inStyle
                      : AppStyle.outStyle,
                ),
              ],
            ),
            SizedBox(height: size.height * .01),
            Text(
              '${item.date.year} - ${item.date.month} - ${item.date.day}',
              style: AppStyle.movementDate,
            )
          ],
        ),
      ),
    );
  }
}
