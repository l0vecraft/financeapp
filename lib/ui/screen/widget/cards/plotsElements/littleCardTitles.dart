import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

class LittleCardTitle {
  final FinanceBloc bloc;
  LittleCardTitle({this.bloc});
  getTitles() => FlTitlesData(
      show: true,
      leftTitles: SideTitles(
        showTitles: true,
        interval: bloc.maxAmount / 4,
        reservedSize: 30,
        getTextStyles: (value) => TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 11,
          color: Colors.black,
        ),
        getTitles: (value) => NumberFormat.compact().format(value),
      ),
      bottomTitles: SideTitles(
        interval: 3,
        showTitles: true,
        getTextStyles: (value) => TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 10,
          color: Colors.black,
        ),
        rotateAngle: -40,
        getTitles: (value) {
          switch (value.toInt()) {
            case 0:
              return 'ENE';
              break;
            case 3:
              return 'ABR';
              break;
            case 6:
              return 'JUL';
              break;
            case 9:
              return 'OCT';
              break;
            default:
              return '';
          }
        },
      ));
}
