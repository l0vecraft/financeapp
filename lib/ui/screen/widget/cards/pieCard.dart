import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

class PieCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.height * .02, horizontal: size.width * .01),
      child: PieChart(PieChartData(
          pieTouchData: PieTouchData(
            enabled: true,
            touchCallback: (touchResponse) {
              if (touchResponse.touchInput is FlLongPressEnd ||
                  touchResponse.touchInput is FlPanEnd) {}
            },
          ),
          borderData: FlBorderData(show: false),
          sections: [
            PieChartSectionData(
              color: Colors.green,
              showTitle: true,
              title: '40%',
              value: 40,
              titleStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            PieChartSectionData(
              color: Colors.blue,
              showTitle: true,
              title: '20%',
              value: 20,
              titleStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            PieChartSectionData(
              color: Colors.purple,
              showTitle: true,
              title: '30%',
              value: 30,
              titleStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            PieChartSectionData(
              color: Colors.pink,
              showTitle: true,
              title: '10%',
              value: 10,
              titleStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
          ])),
    );
  }
}
