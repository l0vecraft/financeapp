import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/constants.dart';
import 'package:finance_app/ui/screen/widget/cards/plotsElements/littleCardTitles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:get/get.dart';

class PlotCard extends StatefulWidget {
  @override
  _PlotCardState createState() => _PlotCardState();
}

class _PlotCardState extends State<PlotCard> {
  final FinanceBloc _bloc = Get.find<FinanceBloc>();
  List<FlSpot> _spotsI = [];
  @override
  void initState() {
    super.initState();
    // _spotsI = _bloc.incomingSpots.value ?? [];
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.height * .02, horizontal: size.width * .01),
      child: LineChart(
        LineChartData(
            minX: 0,
            maxX: 11,
            minY: 0,
            maxY: _bloc.maxAmount.value,
            titlesData: LittleCardTitle(bloc: _bloc).getTitles(),
            gridData: FlGridData(
              getDrawingHorizontalLine: (value) {
                return FlLine(
                  color: Colors.grey,
                  strokeWidth: .5,
                );
              },
              drawVerticalLine: true,
              getDrawingVerticalLine: (value) =>
                  FlLine(strokeWidth: .5, color: Colors.grey),
            ),
            borderData: FlBorderData(show: false),
            lineBarsData: [
              LineChartBarData(
                  spots: [
                    FlSpot(0, 100000),
                    FlSpot(1, 3500000),
                    FlSpot(2, 500000),
                    FlSpot(3, 10000),
                    FlSpot(4, 1540000),
                    FlSpot(5, 340000),
                    FlSpot(6, 2560000),
                    FlSpot(7, 50000),
                    FlSpot(8, 2000),
                    FlSpot(9, 1000000),
                    FlSpot(10, 1500000),
                    FlSpot(11, 100000),
                  ],
                  isCurved: true,
                  colors: AppStyle.incomingColors,
                  belowBarData: BarAreaData(
                    show: true,
                    colors: AppStyle.inColorsArea,
                  ),
                  dotData: FlDotData(show: false)),
              LineChartBarData(
                  spots: [
                    FlSpot(0, 2560000),
                    FlSpot(1, 50000),
                    FlSpot(2, 2000),
                    FlSpot(3, 1000000),
                    FlSpot(4, 1500000),
                    FlSpot(5, 100000),
                    FlSpot(6, 7100000),
                    FlSpot(7, 3500000),
                    FlSpot(8, 500000),
                    FlSpot(9, 10000),
                    FlSpot(10, 1540000),
                    FlSpot(11, 340000),
                  ],
                  isCurved: true,
                  colors: AppStyle.outComingColors,
                  belowBarData: BarAreaData(
                    colors: AppStyle.outColorsArea,
                    show: true,
                  ),
                  dotData: FlDotData(show: false))
            ],
            lineTouchData: LineTouchData(
                enabled: true,
                touchTooltipData: LineTouchTooltipData(
                  tooltipBgColor: Colors.purple,
                  tooltipRoundedRadius: 30,
                  getTooltipItems: (touchedSpots) => touchedSpots
                      .map((e) => LineTooltipItem(
                            '\$ ${NumberFormat.decimalPattern().format(e.y)}',
                            TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ))
                      .toList(),
                ))),
      ),
    );
  }
}
// child: LineChart(LineChartData(
//     //* Estos seran los meses
//     minX: 0,
//     maxX: 11,
//     //* Estos seran los valores que ingrese
//     minY: 0,
//     maxY:
//         _bloc.maxAmount.value, //* este valor hay que calcularlo siempre
//     //* ahora como son meses y dinero, debe llevar un estilo
//     //*(mes, cantidad)
//     titlesData: LittleCardTitle(bloc: _bloc).getTitles(),
//     gridData: FlGridData(
//       verticalInterval: 4,
//       horizontalInterval: 5,
//       show: true,
//       getDrawingHorizontalLine: (value) {
//         return FlLine(
//           color: Colors.grey,
//           strokeWidth: .5,
//         );
//       },
//       drawVerticalLine: true,
//       getDrawingVerticalLine: (value) {
//         return FlLine(
//           color: Colors.grey,
//           strokeWidth: .5,
//         );
//       },
//     ),
//     borderData: FlBorderData(show: false),
//     lineTouchData: LineTouchData(
//         enabled: true,
//         touchTooltipData: LineTouchTooltipData(
//           tooltipBgColor: Colors.purple,
//           tooltipRoundedRadius: 30,
//           getTooltipItems: (touchedSpots) => touchedSpots
//               .map((e) => LineTooltipItem(
//                   '\$ ${NumberFormat.decimalPattern().format(e.y)}',
//                   TextStyle(
//                       color: Colors.white,
//                       fontWeight: FontWeight.w600)))
//               .toList(),
//         )),
//     lineBarsData: [
//       LineChartBarData(
//         spots: [
//           //* este sera el que maneje las coordenadas
//           FlSpot(0, 30000),
//           FlSpot(1, 5000),
//           FlSpot(2, 66000),
//           FlSpot(3, 30000),
//           FlSpot(4, 5000),
//           FlSpot(5, 66000),
//           FlSpot(5, 30000),
//           FlSpot(8, 5000),
//           FlSpot(9, 66000),
//         ],
//         isCurved: true,
//         colors: AppStyle.colors,
//         barWidth: size.width * .007,
//         dotData: FlDotData(
//           show: false,
//         ),
//       ),
//     ])),
