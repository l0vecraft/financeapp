import 'package:finance_app/ui/screen/widget/cards/BalanceOptions.dart';
import 'package:finance_app/ui/screen/widget/headers/BalanceHeader.dart';
import 'package:flutter/material.dart';

class OptionsCard extends StatefulWidget {
  @override
  _OptionsCardState createState() => _OptionsCardState();
}

class _OptionsCardState extends State<OptionsCard>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [BalanceHeader(), BalanceOptions()],
      ),
    );
  }
}
