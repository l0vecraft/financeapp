import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/screen/widget/dialogs/financeDialog.dart';
import 'package:finance_app/ui/screen/widget/items/item_options.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BalanceOptions extends StatefulWidget {
  @override
  _BalanceOptionsState createState() => _BalanceOptionsState();
}

class _BalanceOptionsState extends State<BalanceOptions>
    with SingleTickerProviderStateMixin {
  final FinanceBloc _financeBloc = Get.find<FinanceBloc>();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ItemOptions(
            title: 'Registrar ingresos.',
            text: 'New Incoming',
            icon: 'assets/dividendo.svg',
            type: TypeMovement.income,
          ),
          ItemOptions(
            text: 'New Outcoming',
            title: 'Registrar egresos.',
            icon: 'assets/bancarrota.svg',
            type: TypeMovement.outcome,
          ),
          ItemOptions(
            text: 'Check Saved',
            icon: 'assets/ahorro.svg',
            hasDialog: false,
          ),
        ],
      ),
    );
  }
}
