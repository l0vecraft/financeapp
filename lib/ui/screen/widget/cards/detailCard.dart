import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/constants.dart';
import 'package:finance_app/ui/screen/widget/cards/plotsElements/littleCardTitles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';

class DetailCard extends StatefulWidget {
  @override
  _DetailCardState createState() => _DetailCardState();
}

class _DetailCardState extends State<DetailCard> {
  final FinanceBloc _bloc = Get.find<FinanceBloc>();
  List<FlSpot> _inSpots = [];
  List<FlSpot> _outSpots = [];

  @override
  void initState() {
    super.initState();
    _bloc.incomingSpots.forEach((element) {
      _inSpots.add(element);
    });
    _bloc.outcomingSpots.forEach((element) {
      _outSpots.add(element);
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.height * .02, horizontal: size.width * .01),
      child: Card(
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: LineChart(
            LineChartData(
                maxX: 11,
                minX: 0,
                minY: 0,
                maxY: _bloc.maxAmount.value,
                borderData: FlBorderData(show: false),
                titlesData: LittleCardTitle(bloc: _bloc).getTitles(),
                gridData: FlGridData(
                    drawVerticalLine: true,
                    getDrawingHorizontalLine: (value) {
                      return FlLine(color: Colors.grey, strokeWidth: .5);
                    },
                    getDrawingVerticalLine: (value) =>
                        FlLine(color: Colors.grey, strokeWidth: .5)),
                lineBarsData: [
                  LineChartBarData(
                    spots: _inSpots,
                    isCurved: true,
                    colors: AppStyle.incomingColors,
                    dotData: FlDotData(show: false),
                  ),
                  LineChartBarData(
                    spots: _outSpots,
                    isCurved: true,
                    colors: AppStyle.outComingColors,
                    dotData: FlDotData(show: false),
                  )
                ],
                lineTouchData: LineTouchData(
                  enabled: true,
                  touchTooltipData: LineTouchTooltipData(
                      tooltipBgColor: Colors.purple,
                      tooltipRoundedRadius: 30,
                      getTooltipItems: (items) => items
                          .map((e) => LineTooltipItem(
                                '\$ ${NumberFormat.decimalPattern().format(e.y)}',
                                TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ))
                          .toList()),
                )),
          ),
        ),
      ),
    );
  }
}
