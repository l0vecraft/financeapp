import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/core/models/categorias.dart';
import 'package:finance_app/core/models/transaccion.dart';
import 'package:intl/intl.dart';
import 'package:finance_app/ui/constants.dart';
import 'package:finance_app/ui/screen/widget/inputs/movementInput.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:pattern_formatter/pattern_formatter.dart';

enum TypeMovement { income, outcome }

class FinanceDialog extends StatefulWidget {
  final String title;
  final TypeMovement type;

  FinanceDialog({this.title, this.type});

  @override
  _FinanceDialogState createState() => _FinanceDialogState();
}

class _FinanceDialogState extends State<FinanceDialog>
    with SingleTickerProviderStateMixin {
  final FinanceBloc _bloc = Get.find<FinanceBloc>();
  final _formKey = GlobalKey<FormState>();

  Animation _animation;
  Categoria _valueSelected;

  AnimationController _animationController;
  TextEditingController _title = TextEditingController();
  TextEditingController _descrip = TextEditingController();
  TextEditingController _quantity = TextEditingController();
  TextEditingController _category = TextEditingController();

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 600));
    _animation = Tween<Offset>(begin: Offset(0, 2), end: Offset(0, 0)).animate(
        CurvedAnimation(
            parent: _animationController, curve: Curves.easeInOutBack));

    _animationController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _title.dispose();
    _quantity.dispose();
    _category.dispose();
    _descrip.dispose();
  }

  _onSave() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      var tmp = Transaccion(
          category: widget.type == TypeMovement.income
              ? 'Trabajo'
              : _bloc.category.type,
          date: DateTime.now(),
          description: _descrip.text,
          id: '1',
          quantity: NumberFormat().parse(_quantity.text),
          title: _title.text.capitalizeFirst,
          type: widget.type == TypeMovement.income ? 'incoming' : 'outcoming');
      if (widget.type == TypeMovement.income) {
        var _spot = FlSpot(tmp.date.month.toDouble() - 1, tmp.quantity);
        _bloc.addNewInSpot(_spot);
      } else {
        var _spot = FlSpot(tmp.date.month.toDouble() - 1, tmp.quantity);
        _bloc.addNewOutSpot(_spot);
      }
      // await _bloc.addData(tmp);
      CollectionReference data =
          FirebaseFirestore.instance.collection('movements');
      data
          .add(tmp.toJson())
          .then((value) => print('dato agregado'))
          .printError(info: 'error aqui');
      _quantity.clear();
      _title.clear();
      _descrip.clear();
      _category.clear();
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return SlideTransition(
      position: _animation,
      child: Dialog(
        insetAnimationCurve: Curves.bounceInOut,
        insetAnimationDuration: Duration(milliseconds: 500),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        insetPadding: EdgeInsets.symmetric(horizontal: size.width * .08),
        child: Container(
          height: size.height * .7,
          child: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.symmetric(vertical: size.height * .02),
              children: [
                Text(
                  widget.title,
                  style: AppStyle.dialogTitle,
                  textAlign: TextAlign.center,
                ),
                MovementInput(
                  controller: _title,
                  size: size,
                  text: widget.type == TypeMovement.income
                      ? 'Titulo del ingreso'
                      : 'Titulo del gasto',
                  onSave: (String value) => setState(() {
                    _title.text = value;
                  }),
                ),
                MovementInput(
                  controller: _descrip,
                  size: size,
                  text: 'Descripcion',
                ),
                MovementInput(
                  controller: _quantity,
                  size: size,
                  text: 'Cantidad',
                  prefix: true,
                  prefixText: '\$',
                  textInputType: TextInputType.number,
                  formaters: [ThousandsFormatter(allowFraction: true)],
                  onSave: (String value) => setState(() {
                    _quantity.text = value;
                  }),
                ),
                widget.type == TypeMovement.income
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: size.height * 0.02,
                            horizontal: size.width * .05),
                        child: DropdownButtonFormField<Categoria>(
                          value: _valueSelected,
                          hint: Text(
                            _valueSelected == null
                                ? 'Selecciona una categoria'
                                : _valueSelected.type,
                            style: TextStyle(color: Colors.purple),
                          ),
                          decoration: AppStyle.fieldDecoration,
                          onChanged: (value) {
                            _bloc.category = value;
                          },
                          items: categorias
                              .map((e) => DropdownMenuItem<Categoria>(
                                    child: Text(e.type),
                                    value: e,
                                  ))
                              .toList(),
                        ),
                      ),
                SizedBox(
                  height: size.height * .05,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                          onPressed: () => Navigator.of(context).pop(),
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.grey[200],
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(size.height * .015),
                            child: Text(
                              'Cancelar',
                              style: AppStyle.cancelText,
                            ),
                          )),
                      SizedBox(
                        width: size.width * .04,
                      ),
                      ElevatedButton(
                          onPressed: _onSave,
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.purple),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10)))),
                          child: Padding(
                            padding: EdgeInsets.all(size.height * .015),
                            child: Text(
                              'Registrar',
                              style: AppStyle.saveText,
                            ),
                          ))
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
