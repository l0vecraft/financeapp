import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class BalanceHeader extends StatelessWidget {
  final FinanceBloc _financeBloc = Get.find<FinanceBloc>();
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding:
            EdgeInsets.symmetric(vertical: ScreenUtil().uiSize.height * .01),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: ScreenUtil().uiSize.width * .02),
              child: Text(
                'My Balance',
                style: AppStyle.headerFinance,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: ScreenUtil().uiSize.width * .02),
              child: Obx(
                () => Text(
                  'COP \$ ${NumberFormat().format(_financeBloc.balance)}',
                  style: AppStyle.amount,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
