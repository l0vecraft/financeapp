import 'package:finance_app/ui/constants.dart';
import 'package:flutter/material.dart';

class ShapePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..strokeWidth = 5
      ..strokeCap = StrokeCap.round
      ..shader = LinearGradient(
              colors: AppStyle.colors,
              begin: Alignment.topRight,
              end: Alignment.bottomLeft)
          .createShader(Rect.fromPoints(
              Offset(size.width / 2, 0), Offset(0, size.height / 2)))
      ..style = PaintingStyle.fill;

    final path = Path();
    path.lineTo(0, size.height * .30);
    path.quadraticBezierTo(
        size.width * .50, size.height * .65, size.width, size.height * .3);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
