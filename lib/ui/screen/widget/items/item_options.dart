import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:finance_app/ui/constants.dart';
import 'package:finance_app/ui/screen/widget/dialogs/financeDialog.dart';

class ItemOptions extends StatefulWidget {
  final String text;
  final String icon;
  final String title;
  final TypeMovement type;
  final bool hasDialog;

  const ItemOptions({
    Key key,
    this.text,
    this.icon,
    this.title,
    this.type,
    this.hasDialog = true,
  }) : super(key: key);

  @override
  _ItemOptionsState createState() => _ItemOptionsState();
}

class _ItemOptionsState extends State<ItemOptions>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;
  Animation _secondAnimation;
  Animation _thirdAnimation;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween<Offset>(begin: Offset(0, 0), end: Offset(0, -1)).animate(
        CurvedAnimation(
            parent: _animationController,
            curve: Interval(0, .6, curve: Curves.easeIn)));
    _secondAnimation = Tween<Offset>(begin: Offset(0, 0), end: Offset(.25, 0))
        .animate(CurvedAnimation(
            parent: _animationController,
            curve: Interval(.70, .84, curve: Curves.bounceInOut)));
    _thirdAnimation = Tween<Offset>(begin: Offset(0, 0), end: Offset(-.5, 0))
        .animate(CurvedAnimation(
            parent: _animationController,
            curve: Interval(.85, 1.0, curve: Curves.bounceInOut)));
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        _animationController.forward();
        await Future.delayed(Duration(milliseconds: 300));
        _animationController.reverse();
        await Future.delayed(Duration(milliseconds: 200));
        if (widget.hasDialog) {
          showDialog(
              context: context,
              builder: (context) => FinanceDialog(
                    title: widget.title,
                    type: widget.type,
                  ));
        }
      },
      child: Column(
        children: [
          SlideTransition(
            position: _animation,
            child: SlideTransition(
              position: _secondAnimation,
              child: SlideTransition(
                position: _thirdAnimation,
                child: SvgPicture.asset(
                  widget.icon,
                  height: ScreenUtil().uiSize.height * .03,
                ),
              ),
            ),
          ),
          SizedBox(height: 6),
          Text(
            '${widget.text}',
            style: AppStyle.itemTitle,
          ),
        ],
      ),
    );
  }
}
