import 'package:finance_app/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MovementInput extends StatelessWidget {
  const MovementInput(
      {Key key,
      @required this.size,
      @required this.text,
      @required this.controller,
      this.prefix = false,
      this.textInputType = TextInputType.text,
      this.prefixText,
      this.onSave(String value),
      this.formaters})
      : super(key: key);

  final Size size;
  final String text;
  final bool prefix;
  final TextEditingController controller;
  final String prefixText;
  final TextInputType textInputType;
  final List<TextInputFormatter> formaters;
  final Function onSave;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: size.height * 0.02, horizontal: size.width * .05),
      child: TextFormField(
        controller: controller,
        keyboardType: textInputType,
        cursorColor: Colors.purple,
        style: TextStyle(color: Colors.grey[600]),
        onSaved: onSave,
        inputFormatters: formaters ?? [],
        validator: (value) {
          if (value.isEmpty)
            return 'Este campo es obligatorio y no puede quedar vacio.';
          return null;
        },
        decoration: AppStyle.fieldDecoration
            .copyWith(labelText: text, prefixText: prefix ? prefixText : null),
      ),
    );
  }
}
