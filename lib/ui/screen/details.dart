import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:finance_app/ui/screen/widget/cards/detailCard.dart';
import 'package:finance_app/ui/screen/widget/cards/plotCard.dart';
import 'package:finance_app/ui/screen/widget/painters/shapePaint.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            CustomPaint(
              foregroundPainter: ShapePaint(),
              child: Container(),
            ),
            Positioned(
              left: size.width * .009,
              top: size.height * .006,
              child: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ),
            Positioned(
              top: size.height * .15,
              left: size.width * .05,
              child: Container(
                height: size.height * .35,
                width: size.width * .9,
                child: DetailCard(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
