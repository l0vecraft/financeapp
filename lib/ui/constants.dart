import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppStyle {
  static List<Color> colors = [Color(0xFFc722bf), Color(0xff8822c7)];
  static List<Color> incomingColors = [Color(0xFF0697F8), Color(0xFF1CBDDA)];
  static List<Color> inColorsArea = [
    Color(0xFF0697F8).withOpacity(.2),
    Color(0xFF1CBDDA).withOpacity(.2)
  ];
  static List<Color> outComingColors = [Color(0xFFD629AB), Color(0xFFF118DF)];
  static List<Color> outColorsArea = [
    Color(0xFFD629AB).withOpacity(.2),
    Color(0xFFF118DF).withOpacity(.2)
  ];
  static TextStyle headerFinance =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.grey);
  static TextStyle amount =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.black);
  static TextStyle itemTitle = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w600, color: Colors.grey[700]);
  static TextStyle movemtnTitle = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey[700]);
  static TextStyle movementDesc = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w500, color: Colors.grey[700]);
  static TextStyle inStyle = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w600, color: Color(0xFF18A50B));
  static TextStyle outStyle = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w600, color: Color(0xFFD10808));
  static TextStyle movementDate = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w600, color: Colors.grey[700]);
  static TextStyle dialogTitle = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w700, color: Colors.grey[700]);
  static TextStyle cancelText = TextStyle(
      fontSize: 15, fontWeight: FontWeight.bold, color: Colors.grey[700]);
  static TextStyle saveText =
      TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white);
  static InputDecoration fieldDecoration = InputDecoration(
      labelStyle: TextStyle(color: Colors.purple),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Colors.grey)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Colors.purple)));
}
