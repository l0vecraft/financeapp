import 'package:finance_app/routes/routes.dart';
import 'package:finance_app/ui/screen/home.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _fireBase = Firebase.initializeApp(name: 'finance');
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fireBase,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print('The error: ${snapshot.error}');
          // return Text('Algo fue mal');
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return ScreenUtilInit(
            designSize: Size(1080, 1920),
            allowFontScaling: false,
            child: GetMaterialApp(
              debugShowCheckedModeBanner: false,
              home: Home(),
              getPages: FinanceRoutes().routes,
            ),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
