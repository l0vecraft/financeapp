import 'package:finance_app/core/controller/finance/financeBinding.dart';
import 'package:finance_app/ui/screen/home.dart';
import 'package:get/get.dart';

class FinanceRoutes {
  final routes = [
    GetPage(name: '/home', binding: FinanceBinding(), page: () => Home()),
  ];
}
