import 'package:finance_app/core/controller/financeBloc.dart';
import 'package:get/get.dart';

class FinanceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FinanceBloc());
  }
}
