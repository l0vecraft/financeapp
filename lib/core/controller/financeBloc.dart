import 'package:finance_app/core/models/categorias.dart';
import 'package:finance_app/core/models/transaccion.dart';
import 'package:finance_app/core/repository/api_finance.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';

class FinanceBloc extends GetxController {
  final ApiFinance _apiFinance = ApiFinance();

  Rx<GlobalKey<AnimatedListState>> _key = GlobalKey<AnimatedListState>().obs;
  RxList<Transaccion> _movements = <Transaccion>[].obs;
  Rx<Categoria> _category = Categoria().obs;
  RxList _incomingSpots = [].obs;
  RxList _outcomingSpots = [].obs;
  RxDouble _balance = 0.0.obs;
  RxDouble _maxAmount = 10000000.0.obs;
  // RxDouble _maxOutComing = 0.0.obs;

  RxList<Transaccion> get movements => _movements;
  Categoria get category => _category.value;
  RxDouble get balance => _balance;
  RxDouble get maxAmount => _maxAmount;
  RxList get incomingSpots => _incomingSpots;
  RxList get outcomingSpots => _outcomingSpots;
  // RxDouble get maxOutcome => _maxOutComing;

  set maxAmount(newValue) => _maxAmount.value = newValue;
  set category(Categoria newValue) => _category.update((val) {
        val.id = newValue.id;
        val.type = newValue.type;
      });

  GlobalKey<AnimatedListState> get key => _key.value;

  set key(GlobalKey<AnimatedListState> newValue) => _key.value = newValue;

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  void _initData() async {
    Future.delayed(Duration(seconds: 1));
    _movements.add(
      Transaccion(
          category: 'prueba',
          date: DateTime.parse('2021-01-21'),
          // date: DateTime.now(),
          description: 'Descripcion de un ingreso',
          id: '1',
          quantity: NumberFormat().parse('10000000'),
          title: 'Prueba ingreso',
          type: 'incoming'),
    );
    _movements.add(
      Transaccion(
          category: 'prueba',
          date: DateTime.parse('2021-01-21'),
          // date: DateTime.now(),
          description: 'Descripcion de un egreso',
          id: '2',
          quantity: NumberFormat().parse('5000000'),
          title: 'Prueba egreso',
          type: 'outcoming'),
    );
    _movements.forEach((element) {
      double _tmp = element.date.month.toDouble() - 1;
      if (element.type == 'incoming') {
        _balance.value += element.quantity;
        addNewInSpot(FlSpot(_tmp, element.quantity));
      } else {
        _balance.value -= element.quantity;
        addNewOutSpot(FlSpot(_tmp, element.quantity));
      }
    });
  }

  Future addData(Transaccion transaccion) async {
    await _apiFinance.newMovement(transaccion);
    if (_key.value.currentState != null) {
      _key.value.currentState
          .insertItem(0, duration: Duration(milliseconds: 500));
    }

    _movements.add(transaccion);
    double _tmp = transaccion.date.month.toDouble() - 1;
    if (transaccion.type == 'incoming') {
      _balance.value += transaccion.quantity;
      addNewInSpot(FlSpot(_tmp, transaccion.quantity));
    } else {
      _balance.value -= transaccion.quantity;
      addNewOutSpot(FlSpot(_tmp, transaccion.quantity));
    }
  }

  void removeData(int index) async {
    Future.delayed(Duration(seconds: 1));
    _movements.removeAt(index);
  }

  void addNewInSpot(FlSpot value) {
    _incomingSpots.add(value);
  }

  void addNewOutSpot(FlSpot value) {
    _outcomingSpots.add(value);
  }
  // void getMaxValue(TypeMovement type) {
  //   if(type == TypeMovement.income){
  //     var tmp =_movements.reduce((previousValue, nextElement) =>
  //       previousValue.quantity >= nextElement.quantity
  //           ? previousValue
  //           : nextElement);

  //   }

  // }
}
