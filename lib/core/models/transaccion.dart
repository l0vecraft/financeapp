class Transaccion {
  String id;
  String type;
  double quantity;
  DateTime date;
  String title;
  String description;
  String category;

  Transaccion({
    this.id,
    this.category,
    this.date,
    this.description,
    this.quantity,
    this.title,
    this.type,
  });
  Transaccion.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? '';
    date = json['date'] ?? DateTime.now();
    description = json['description'] ?? '';
    quantity = json['quantity'] ?? 0.0;
    title = json['title'] ?? '';
    type = json['type'] ?? '';
    category = json['category'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this.id ?? '';
    data['date'] = this.date ?? DateTime.now();
    data['description'] = this.description ?? '';
    data['quantity'] = this.quantity ?? 0.0;
    data['title'] = this.title ?? '';
    data['type'] = this.type ?? '';
    data['category'] = this.category ?? '';
    return data;
  }

  @override
  String toString() {
    return '{id:$id, date:$date,categoria:$category, description:$description, cantidad:$quantity, titulo:$title, type:$type}';
  }
}
