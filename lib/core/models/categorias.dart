class Categoria {
  int id;
  String type;
  Categoria({
    this.id,
    this.type,
  });
}

List<Categoria> categorias = [
  Categoria(id: 1, type: 'prueba'),
  Categoria(id: 2, type: 'hogar'),
  Categoria(id: 3, type: 'estudios'),
  Categoria(id: 4, type: 'servicios'),
  Categoria(id: 5, type: 'ropa'),
  Categoria(id: 6, type: 'mascota'),
  Categoria(id: 7, type: 'fiesta'),
  Categoria(id: 8, type: 'comida'),
  Categoria(id: 9, type: 'personal'),
  Categoria(id: 10, type: 'trabajos'),
];
